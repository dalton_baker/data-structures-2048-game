/**************************************************************************//**
 * @file
 * @brief This file contains the members of the Player class.
 * This was provided by Dr. Hinker
 *****************************************************************************/
#include "player.h"

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * The constructor for the player class.
 * This code was provided by Dr. Hinker.
 *
 * @param[in]  game - the current game being played
 *
 *****************************************************************************/
Player::Player(Game *game) : game(game) { }


/**************************************************************************//**
 * A Player need only be able to make a valid move.  The key is creating a
 * Player that makes good moves and eventually reaches 2048 tile.  This is
 * a reference implementation which is not smart.  It simply tries random
 * moves until it discovers a valid move and then it picks that move.
 *
 * Your implementation will need to be much smarter.
 * **************(*************************************************************/
void Player::makeMove()
{
    //leave if the game is done
    if (game->isGameOver())
    {
        return;
    }

    //get the current game board
    Board board(game->getBoard());

    //this variably only exists to satisfy the requirements of the
    //permutation function. It doesn't serve any other purpose.
    long long throwaway;

    //call the permutation function and make whatever move it returns.
    game->makeMove(permuteFindMove(board, 4, throwaway));
}
