/**************************************************************************//**
 * @file
 * @brief Header file for the funcrtions we created
 *****************************************************************************/
#ifndef __PERMUTATION__H__
#define __PERMUTATION__H__
#include "game.h"

ValidMove permuteFindMove(Board board, int numOfPerm, long long &score);
bool checkMoveNoChange(ValidMove move, Board board);
long long getBoardScore(Board board);
void printBoard(Board board);

#endif
