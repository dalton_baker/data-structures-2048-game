#include "game.h"

Game::Game() : gameOver(false), score(0) { }

bool Game::makeMove(ValidMove move)
{
   static int invalidCount = 0;
   bool retVal;
   if ((gameOver = isGameOver()) == true)
      retVal = false;
   else
   {
      retVal = board.makeMove(move);
      if (!retVal)
      {
         invalidCount++;
         if (invalidCount > 10) gameIsOver();
      }
      else
         invalidCount = 0;

      score = board.getScore();
   }
   return retVal;
}

void Game::gameIsOver() { gameOver = true; }

bool Game::isGameOver()
{
   gameOver = true;
   for (ValidMove move : {DOWN, LEFT, RIGHT, UP})
   {
       Board temp(board);
       if (temp.checkMove(move))
          gameOver = false;
   }
   return gameOver;
}

Board Game::getBoard()
{
   Board tmp(board);
   return tmp;
}

int Game::getScore() { return score ; }

void Game::draw() { }
