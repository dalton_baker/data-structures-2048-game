/**************************************************************************//**
 * @file
 * @brief Where the permutations take place
 *****************************************************************************/

#include "permutation.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will check all 4 moves to find the best possible move. It will
 * call itself recursively to check the next several moves after the current
 * move. It will add up scores from each permutation and find the path with the
 * highest score. It will eventually return the move at the top of the highest
 * scoring combination of moves.
 *
 * @param[in] board - The board we are checking
 *
 * @param[in] numOfPerm - The number of permutations deep we want to go
 *
 * @param[in] score - The score inside the permuted board. Only used inside the
 *                    permutation, does not have any use outside the functrion
 *
 * @return - The final move that was picked
 *
 *****************************************************************************/
ValidMove permuteFindMove(Board board, int numOfPerm, long long &score)
{
    ValidMove finalDecision = DOWN;
    long long scoreTracker = 0;

    //stop the permutation if we reach the maximum number that we want
    if(numOfPerm < 0)
    {
        //returning down does not do anything at all in this case
        //this is completely arbitrary
        return DOWN;
    }

    //loop through each possible move
    for (ValidMove move : {
                DOWN, LEFT, RIGHT, UP
            })
    {
        Board permuteBoard(board);
        long long permuteScore = 0;

        //check if the move can be made and permute through possible moves
        if (checkMoveNoChange(move, permuteBoard))
        {
           //get the current score of the board and shift it left
           //shifting left gives it weight
            permuteScore = getBoardScore(permuteBoard) << numOfPerm;

            //make the move we are checking for
            permuteBoard.makeMove(move);

            //run a permutation on the board to get the next highest score down
            permuteFindMove(permuteBoard, numOfPerm-1, permuteScore);

            //if the score is greater than the highest score, set the high score
            //to the new highest score and make the final decision score to the
            //move that got it that high score.
            if (permuteScore > scoreTracker)
            {
                scoreTracker = permuteScore;
                finalDecision = move;
            }
        }
    }

    //return the highest score found
    score += scoreTracker;

    //return trhe final decision, this only matters for the top permutation
    return finalDecision;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function that will check if a move can be done without changing the
 * actual board
 *
 * @param[in] move - move we are checking
 *
 * @param[in] board - the board we are checking the move on
 *
 * @return - true if the move is valid, false if it isn't
 *
 *****************************************************************************/
bool checkMoveNoChange(ValidMove move, Board board)
{
    //create a new board from our board
    Board checker(board);

    //check the move on the throwaway board
    return checker.checkMove(move);
}


/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will loop through and get the score of the board. It will loop
 * through creating a snake pattern which will do (cellval) ^ 15..14...13 and
 * so on.
 *
 * @param[in] board - the board with all of the tiles in there
 *
 * @return score - the score of the board
 *
 *****************************************************************************/
long long getBoardScore(Board board)
{
    //use this to calculate the weight of each tile
    int weight = 15;
    long long score = 0;

    //loop through each row, starting with the bottom one
    for(int i = 3; i >= 0; i--)
    {
        //cycle through every other row going right to left
        if( i % 2 == 1)
        {
            for(int j = 3; j >= 0; j--)
            {
               //increase the weight of each tile and add the score
               score += board.getCellVal(i, j) << weight--;
            }
        }

        //cycle through every other row going left to right
        else
        {
            for(int j = 0; j < 4; j++)
            {
               //increase the weight of each tile and add the score
               score += board.getCellVal(i, j) << weight--;
            }
        }
    }

    //return the score
    return score;
}
