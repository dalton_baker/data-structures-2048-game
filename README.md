Up until 10:00 on Thursday, our code was much much different.
Our algorithm would try to keep the largest number in the bottom right corner.
As a team, we started writing an algorithm separately however but we eventually
ended up doing the same thing.From there on, one person would write the 
functions to handle or check for special cases, and the other would implement
these functions. This made it so we weren't working in the same file at the
same time. We would regularly talk to each other about what special cases 
needed to be addressed and if any functions needed to change how they were
handling the checks. This made for an more efficient approach to the program
 rather than trying to come up with algorithms separately.


The first strategy we tried was to make a move
and then have the board calculate the next move it should do. This was based on
the idea that any move made should be based off of the move made before it.
We created a hierarchy of moves, ordered most important to least important, it
goes like this: down, right, left, up. As you can see, down and right moves take
precedence over all other moves. Also, left moves and up moves are only done in
if they have to be done. This algorithm required us to keep track of the next
move as a static variable (nextMove), which is not ideal. The program would
attempt to make the move that was stored in nextMove and if it couldn't it would
try each one according to the hierarchy until it found one it could do. Also,
after extensive testing, it was discovered that the last move made has virtually
no effect on the next move that needs to be made. This was the algorithm we had
for checkpoint one, but it was abandoned shortly after checkpoint one.

After that failed, we looked through algorithms on the internet for ideas, but
many of them were much more complex than we could implement. However, we did
get an idea after discovering something called a minimax algorithm.
We attempted to permute 10 plays ahead and calculate the highest score from
the combination of plays. This was used to try to determine what initial move
should be made. This, however, didn't get us above a score of 1,000 mainly
because it didn't always take into account randomly placed numbers and how that
could affect the play and score. All of the scores and calculations inside the
permutations ended up being different from the actual scores, so we had a major
divergence from what the permutation calculated and what would actually happen.

Then we came up with the algorithm that we used up until Thrusday. It's loosely
based on the first algorithm but with some tweaks. Instead of calculating the 
next move to be done, we calculate which move should be done and then do it each
time.In order to calculate moves, we created several small functions that analyze the
board and return bools based on specific board data. In the first algorithm we
created a hierarchy of moves (down -> right -> left -> up). We ended up keeping
this same model, giving down the highest priority and up the least priority.
However, after some testing, we discovered that it worked much better if we
flipped the order and eliminated the least desired moves first. So we check if
an up move should be done first, then the left, then the right, and if none of
those are done, a down move is done automatically. The down move has no
calculations at all. The left move is only done if no other move is possible.
So, most of the algorithm is just checking if a left or right move should be
done.

In a little more detail:
The algorithm would first check if has to make a up move. It would only ever do
an up move if there are no other possible moves. We only wanted to make this move
as a last resort. It will then check if a left move can/should be done. To do a
left move, certain requirements must be met. The left move will only happen if
there are diagonals that can be combined with each other or the player cannot
move right now down. This isn't a last resort move, but we only want to make
that kind of move if we have to. After that, we checked if a move could me made
right. We only wanted to make a right move if there there is a combination that
can be made right and diagonally or if there if there is a combination that
can be made right but not down at all. Down moves are the most common. We wanted
it to be this way because we want larger numbers closer to the bottom of the
board. This is the highest priority move.

As part of our algorithm, as soon as the first row filled up, we wanted the
blocks to begin combining right. As soon as the second row filled up, we wanted
the blocks to begin combining left. We agreed that this would be the best way
to approach things because it would mean that larger blocks should combine with
each other in a zig-zag pattern until it reached the bottom right corner. For
example:
If we have a board like: (sometimes this collapses, so just enter the editing
mode to look at it)

 | | | |
 | | | |
 |8| | |
8|16|32|64|

We would want to move left and then down so the 8's would combine. After this,
we could combine the other numbers to get the 128 block.

With our nearly final code, our algorithm did not handle a situation where 
a small block, such as a 2 gets caught in the bottom right corner. In 
theory, we would want to switch how things are combined until we can get
 the 2 up to the large number it's next to.

In general though, our algorithm did not reach the 2048 block. We believe this
is because there are certain special cases our algorithm has not covered yet.
We have been working at adding one special case check at a time when we notice
problems. To do this, we slowed the play speed down and printed out the board
before moves were made. This way, we could go through every play made and see if
our algorithm was working correctly or if there were special cases we needed to
address. This has significantly helped improve our score long term, and the game
is behaving more like we want it too. However, there are times where we are just
dealt a bad hand. If we were playing it, we could probably remedy the mistake, but
it's harder to do that in the game. We would have to change our whole approach to
the game in some instances. 


At the time, we assumed that this approach worked best and we would be sticking 
with it until the end. After talking to two teams, we realized that our origional 
permutation worked way better, but we were approaching it wrong. One of the biggest
problems with the permutation algorithm was that we were using the checkMove function
without knowing that it changed the board. We weren't creating a copy of the playing 
board. We also were not placing any weight on any of the cell spaces, so terrible
moves were easily made without any consideration. 

The permutation enters a for loop of the different moves you can make. It will create a 
copy of the board. It will then call the checkMoveNoChange function with the current 
move the for loop is on and see if the move is possible. If it is, it will get the score 
of the board and shift it left to give the nearer moves higher precenence. It will then 
call the function again to permute through the moves, subtracting 
number of permutations that were made. If the permutation score based on
the moves is greater than the score counter, it will make the permutation
score the highest score and set the finalDecision variable (variable for 
what the move should be) to the move that that accompanies the highest
score. The function will then return the final move decision which is the
move the game will make.

The board is scored by the position the tiles are in. Starting in the bottom right 
corner and going left, we add each tile value together. For each row, we change the 
dirrection we are adding the tiles up in, so the next row up we go left to right. 
This creates a zig-zag pattern, which makes finding combinations easier. Each time a 
tile is added the tile value is shifted right a certain number in order to give 
different tiles different weight in the score. The first tile added is shifted left 
15 times, the next is shifted 14 times, and so on until the last tile is shifted 0 times. 
