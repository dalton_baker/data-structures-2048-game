/**************************************************************************//**
 * @file
 * @brief This is the header file for player.cpp, it was provided by Dr. Hinker
 *****************************************************************************/
#ifndef __PLAYER_H
#define __PLAYER_H
#include "game.h"
#include "permutation.h"

/*!
* @brief The Player class, this is our robot
*/
class Player
{
    /*! The current game */
    Game *game;
public:
    Player (Game *);
    void makeMove();
};

#endif
