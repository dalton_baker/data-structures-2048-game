#include "board.h"

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Constructor for the board class
 *
 *****************************************************************************/
Board::Board() : gameOver(false), score(0)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    srand((time_t)ts.tv_nsec);
    for (int i = 0 ; i < 4 ; i++)
        for (int j = 0 ; j < 4 ; j++)
            board[i][j].num = 0;

    addValue();
    addValue();
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Constructor for the board class
 *
 * @param[in] b - pointer to the board class
 *
 *****************************************************************************/
Board::Board(const Board& b)
{
    for (int i = 0 ; i < 4 ; i++)
        for (int j = 0 ; j < 4 ; j++)
            board[i][j] = b.board[i][j];

    score = b.score;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Returns the value of the tile in the cell given
 *
 * @param[in] i - the row to check
 *
 * @param[in] j - the column to check
 *
 * @return - the value at the i and j value
 *
 *****************************************************************************/

int Board::getCellVal(int i, int j) {
    return board[i][j].num ;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Sets the vlaue of a tile in the i and j value given
 *
 * @param[in] i - the row to place the value
 *
 * @param[in] j - the column to place the value
 *
 * @param[in] val - the value to place in the i and j position
 *
 *****************************************************************************/
void Board::setCellVal(int i, int j, int val) {
    board[i][j].num  = val;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Returns if the game is over or not
 *
 * @return - if the game is over or not
 *
 *****************************************************************************/
bool Board::isGameOver() {
    return gameOver ;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Will go through and try to place a random value in a random cell
 *
 * @return - the tile can be placed in that cell
 *
 *****************************************************************************/
bool Board::addValue()
{
    bool foundEmpty = false;
    vector<Cell*> emptyCells;
    for (int i = 0 ; i < 4 ; i++)
        for (int j = 0 ; j < 4 ; j++)
            if (!board[i][j].num)
            {
                emptyCells.push_back(&board[i][j]);
                foundEmpty = true;
            }

    if (!foundEmpty)
    {
        gameOver = true;
        return false;
    }

    int randCell = rand() % emptyCells.size();
    int num = 2;
    if (rand() % 100 >= 90) num = 4;
    emptyCells[randCell]->num = num;

    return true;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Get the max value on the board
 *
 * @return mavV - the value of the max tile
 *
 *****************************************************************************/
int Board::maxVal()
{
    int maxV = 0;
    for (int i = 0 ; i < 4 ; i++)
        for (int j = 0 ; j < 4 ; j++)
            if (maxV < board[i][j].num)
                maxV = board[i][j].num;

    return maxV;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Returns the score of the game
 *
 * @return score - the score of the game
 *
 *****************************************************************************/
int Board::getScore()
{
    return score ;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Will print the board out
 *
 *****************************************************************************/
void Board::printBoard()
{
    for (int i = 0 ; i < 4 ; i++)
    {
        for (int j = 0 ; j < 4 ; j++)
            cout << setw(5) << board[i][j];
        cout << endl;
    }
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * See if you can make the move
 *
 * @param[in] - the move that wants to be made
 *
 * @return retVal - if the move can be made or not
 *
 *****************************************************************************/
bool Board::makeMove(ValidMove move)
{
    bool retVal = false;
    switch (move)
    {
    case LEFT  :
        retVal = moveLeft();
        break;
    case RIGHT :
        retVal = moveRight();
        break;
    case UP    :
        retVal = moveUp();
        break;
    case DOWN  :
        retVal = moveDown();
        break;
    }
    if (retVal) addValue();
    return retVal;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * See if you can make the move
 *
 * @param[in] move - the move that wants to be made
 *
 * @return retVal - if the move can be made or not
 *
 *****************************************************************************/
bool Board::checkMove(ValidMove move)
{
    bool retVal = false;
    switch (move)
    {
    case LEFT  :
        retVal = moveLeft();
        break;
    case RIGHT :
        retVal = moveRight();
        break;
    case UP    :
        retVal = moveUp();
        break;
    case DOWN  :
        retVal = moveDown();
        break;
    }
    return retVal;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Try to move left
 *
 * @return didMove - the left move could be made
 *
 *****************************************************************************/
bool Board::moveLeft()
{
    bool didMove = false;
    for (int row = 0 ; row < 4 ; row++)
    {
        vector<bool> canChange {true, true, true, true};
        for (int startCol = 1 ; startCol < 4 ; startCol++)
        {
            int col = startCol;
            while (col > 0)
            {
                if (board[row][col-1] == 0 && board[row][col] != 0)
                {
                    board[row][col-1] = board[row][col];
                    board[row][col] = 0;
                    didMove = true;
                }
                else if (board[row][col-1] != 0 && board[row][col-1] == board[row][col] && canChange[col-1] && canChange[col])
                {
                    board[row][col-1] += board[row][col];
                    board[row][col] = 0;
                    canChange[col-1] = false;
                    score += board[row][col-1].num;
                    didMove = true;
                }
                col--;
            }
        }
    }
    return didMove;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Try to move up
 *
 * @return didMove - the up move could be made
 *
 *****************************************************************************/
bool Board::moveUp()
{
    bool didMove = false;
    for (int col = 0 ; col < 4 ; col++)
    {
        vector<bool> canChange {true, true, true, true};
        for (int startRow = 1 ; startRow < 4 ; startRow++)
        {
            int row = startRow;
            while (row > 0)
            {
                if (board[row][col] != 0 && board[row-1][col] == 0)
                {
                    board[row-1][col] = board[row][col];
                    board[row][col] = 0;
                    didMove = true;
                }
                else if (board[row][col] != 0 && board[row][col] == board[row-1][col] && canChange[row-1] && canChange[row])
                {
                    board[row-1][col] += board[row][col];
                    board[row][col] = 0;
                    canChange[row-1] = false;
                    score += board[row-1][col].num;
                    didMove = true;
                }
                row--;
            }
        }
    }
    return didMove;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Try to move down
 *
 * @return didMove - the down move could be made
 *
 *****************************************************************************/
bool Board::moveDown()
{
    bool didMove = false;
    for (int col = 0; col < 4 ; col++)
    {
        vector<bool> canChange {true, true, true, true};
        for (int startRow = 2 ; startRow >= 0 ; startRow--)
        {
            int row = startRow;
            while (row <= 2)
            {
                if (board[row][col] != 0 && board[row+1][col] == 0)
                {
                    board[row+1][col] = board[row][col];
                    board[row][col] = 0;
                    didMove = true;
                }
                else if (board[row][col] != 0 && board[row][col] == board[row+1][col] && canChange[row+1] && canChange[row])
                {
                    board[row+1][col] += board[row][col];
                    board[row][col] = 0;
                    canChange[row+1] = false;
                    score += board[row+1][col].num;
                    didMove = true;
                }
                row++;
            }
        }
    }
    return didMove;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Try to move right
 *
 * @return didMove - the right move could be made
 *
 *****************************************************************************/
bool Board::moveRight()
{
    bool didMove = false;
    for (int row = 0 ; row < 4 ; row++)
    {
        vector<bool> canChange {true, true, true, true};
        for (int startCol = 2 ; startCol >= 0 ; startCol--)
        {
            int col = startCol;
            while (col <= 2)
            {
                if (board[row][col] != 0 && board[row][col+1] == 0)
                {
                    board[row][col+1] = board[row][col];
                    board[row][col] = 0;
                    didMove = true;
                }
                else if (board[row][col] != 0 && board[row][col] == board[row][col+1] && canChange[col+1] && canChange[col])
                {
                    board[row][col+1] += board[row][col];
                    board[row][col] = 0;
                    canChange[col+1] = false;
                    score += board[row][col+1].num;
                    didMove = true;
                }
                col++;
            }
        }
    }
    return didMove;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Overload operator
 *
 * @param[in] os - the operator to overload
 *
 * @param[in] c - the Cell
 *
 * @return os - the overloaded operator
 *
 *****************************************************************************/
ostream& operator<<(ostream& os, Cell c)
{
    os << c.num;
    return os;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Overload operator
 *
 * @param[in] os - the operator to overload
 *
 * @param[in] c - the Cell
 *
 * @return is - the overloaded operator
 *
 *****************************************************************************/
istream& operator>>(istream& is, Cell& c)
{
    is >> c.num;
    return is;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Overload == operator to see if the cell values are the same
 *
 * @param[in] a - the first cell value to compare
 *
 * @param[in] b - the second cell value to compare
 *
 * @return - if the cells are the same
 *
 *****************************************************************************/
bool operator==(const Cell &a, const Cell &b)
{
    return (a.num == b.num);
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * Overload != operator to see if the cell values are the same
 *
 * @param[in] a - the first cell value to compare
 *
 * @param[in] b - the second cell value to compare
 *
 * @return - if the cells are not the same
 *
 *****************************************************************************/
bool operator!=(const Cell &a, const Cell &b)
{
    return !(a.num == b.num);
}
